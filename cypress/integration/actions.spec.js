/// <reference types="Cypress" />
import { url } from '../constants';

context('Actions', () => {
  beforeEach(() => {
    cy.visit(url);
    cy.contains('Settings').click();
  })

  it('should open menu after click on "Settings"', () => {
    const menu = cy.get('.v-menu__content');
    menu.should('be.visible');
  });

  it('should display and hide graphics in block after click on button', () => {
    const buttonShowInBlock = cy.contains('Show in block'); 
    buttonShowInBlock.click();
    buttonShowInBlock.should('contain', 'Clear block');

    const blockWithGraphics = cy.get('.container-fluid');
    blockWithGraphics.should('be.visible');
  });

  it('should open and close modal after click on button', () => {
    const buttonOpenModal = cy.contains('Open modal');
    buttonOpenModal.click();

    const modalWindow = cy.get('.v-dialog');
    modalWindow.should('be.visible');

    const hideModalButton = cy.contains('Hide stats');
    hideModalButton.click();
  });

  it('should switch checkbox to active state', () => {
    const switchElement = cy.contains('Switch');
    switchElement.click();

    const checkBox = cy.get('[type="checkbox"]');
    checkBox.invoke('attr', 'aria-checked').should('contain', true);
  });
});