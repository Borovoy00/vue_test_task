import Vue from 'vue'
import Vuex from 'vuex';
import * as modules from './modules';

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules
});