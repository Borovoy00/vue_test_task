import * as actions from './actions';
import * as mutations from './mutations';

const cryptocurrencies = {
  state: {
    cryptoStats: null 
  },
  getters: {
    getCryptoStats: state => state.cryptoStats
  },
  actions: actions.cryptocurrencies,
  mutations: mutations.cryptocurrencies
}

const graphicsInModal = {
  state: {
    isOpenModal: false,
  },
  getters: {
    getModalOpenStatus: state => state.isOpenModal,
  },
  mutations: mutations.graphicsInModal,
}

const graphicsInBlock = {
  state: {
    isDisplayInBlock: false,
  },
  getters: {
    getDisplayInBlockStatus: state => state.isDisplayInBlock,
  },
  mutations: mutations.graphicsInBlock,
}

export { cryptocurrencies, graphicsInModal, graphicsInBlock};