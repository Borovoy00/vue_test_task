import axios from 'axios';

import { apiUrl } from './constants';

const cryptocurrencies = {
  LOAD_CRYPTOCURRENCIES_DATA: ({ commit }) => {
    axios
      .get(apiUrl)
      .then(response => {
        const { coins } = response.data.data;
        const coinsNumberOfMarkets = coins.map(coin => {
          return {
            numberOfMarkets: coin.numberOfMarkets,
            name: coin.name
          }
        }).slice(0, 5);

        commit('SET_CRYPTOCURRENCIES_DATA', coinsNumberOfMarkets);
      })
      .catch(() => commit('SET_CRYPTOCURRENCIES_DATA', null))
  },
}

export { cryptocurrencies };