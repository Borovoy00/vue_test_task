import * as mutations from '../mutations';

describe('Mutations test', () => {
  describe('cryptocurrencies mutaitions', () => {
    describe('SET_CRYPTOCURRENCIES_DATA mutation', () => {
      it('should change state cryptoStats to new', () => {
        const newValue = 'new';
        const state = { cryptoStats: 'current' };
        const { SET_CRYPTOCURRENCIES_DATA } = mutations.cryptocurrencies;
  
        SET_CRYPTOCURRENCIES_DATA(state, newValue);
  
        expect(state.cryptoStats).toEqual(newValue);
      });
    });
  });

  describe('graphicsInModal mutaitions', () => {
    describe('OPEN_MODAL mutation', () => {
      it('should change state isOpenModal to true', () => {
        const state = { isOpenModal: false };
        const { OPEN_MODAL } = mutations.graphicsInModal;
      
        OPEN_MODAL(state);
  
        expect(state.isOpenModal).toEqual(true);
      });
    });

    describe('CLOSE_MODAL mutation', () => {
      it('should change state isOpenModal to false', () => {
        const state = { isOpenModal: true };
        const { CLOSE_MODAL } = mutations.graphicsInModal;

        CLOSE_MODAL(state);
  
        expect(state.isOpenModal).toEqual(false);
      });
    });
  });

  describe('graphicsInBlock mutaitions', () => {
    describe('graphicsInBlock mutaitions', () => {
      it('should reverse state isDisplayInBlock', () => {
        const state = { isDisplayInBlock: false };
        const { SHOW_HIDE_GRAPHICS_IN_BLOCK } = mutations.graphicsInBlock;
      
        SHOW_HIDE_GRAPHICS_IN_BLOCK(state);
        expect(state.isDisplayInBlock).toEqual(true);
    
        SHOW_HIDE_GRAPHICS_IN_BLOCK(state);
        expect(state.isDisplayInBlock).toEqual(false);
      });
    });
  });
});