const cryptocurrencies = {
  SET_CRYPTOCURRENCIES_DATA: (state, data) => {
    state.cryptoStats = data;
  }
}

const graphicsInModal = {
  OPEN_MODAL: state => {
    state.isOpenModal = true;
  },
  CLOSE_MODAL: state => {
    state.isOpenModal = false;
  }
}

const graphicsInBlock = {
  SHOW_HIDE_GRAPHICS_IN_BLOCK: state => {
    state.isDisplayInBlock = !state.isDisplayInBlock;
  },
}

export { cryptocurrencies, graphicsInModal, graphicsInBlock };